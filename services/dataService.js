class DataService {
  constructor(dataStore) {
    this.dataStore = dataStore;
  }

  findById(id) {
    return this.dataStore.findById(id);
  }

  findAll() {
    return this.dataStore.findAll();
  }
}

let dataService;

export const initDataService = (dataStore) => {
  if (dataService) {
    throw Error('DataService already initialized!');
  }

  dataService = new DataService(dataStore);
};

export const getDataService = () => {
  if (!dataService) {
    throw Error('DataService not initialized!');
  }

  return dataService;
};
