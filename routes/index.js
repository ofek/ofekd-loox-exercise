import express from 'express';
import { getDataService } from '../services/dataService.js';

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.redirect('/1');
});

router.get('/:id', (req, res) => {
  const dataService = getDataService();
  res.render('index', {
    pets: dataService.findAll(),
    selectedId: req.params.id,
  });
});

export default router;
