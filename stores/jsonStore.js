import fs from 'fs';

export class JSONStore {
  constructor(options) {
    const { rawJSON } = options;

    this.rawJSON = rawJSON;
  }

  findById(id) {
    return this.rawJSON[id];
  }

  findAll() {
    return this.rawJSON;
  }

  static fromPath(jsonPath) {
    const rawJSON = JSON.parse(fs.readFileSync(jsonPath).toString());
    return new JSONStore({ rawJSON });
  }
}
