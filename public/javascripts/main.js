const toggles = Array.from(document.getElementsByClassName('toggle'));

toggles.forEach((toggle) => {
  toggle.addEventListener('click', (event) => {
    const toggleClass = 'toggle-expanded';
    const parent = event.currentTarget.parentNode;
    if (parent.className.includes(toggleClass)) {
      parent.className = parent.className
        .split(' ')
        .filter((className) => className !== toggleClass)
        .join(' ');
    } else {
      parent.className = parent.className
        .split(' ')
        .concat([toggleClass])
        .join(' ');
    }
  });
});
